class ApplicationMailer < ActionMailer::Base
  default from: "Cursos FazINOVA <cursos@fazinova.com.br>"
  layout 'mailer'
end
