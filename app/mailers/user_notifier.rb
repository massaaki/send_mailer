class UserNotifier < ApplicationMailer


  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_signup_email(user)
    @user = user
    mail( :to => @user.email,
    :subject => 'Bem-vindo ao desafio de 30 dias de Oportunidades' )
  end


end
